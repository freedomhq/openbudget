import React from 'react'
import PropTypes from 'prop-types'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { createStructuredSelector, createSelector } from 'reselect'
import { App } from 'components'
import { withRouter } from 'react-router-dom'
import * as AppActions from 'actions/app'

const AppContainer = props => (
  <App loading={props.loading} error={props.error}>
    {props.children}
  </App>
)

AppContainer.propTypes = {
  children: PropTypes.element.isRequired,
  loading: PropTypes.bool.isRequired,
  error: PropTypes.oneOfType([PropTypes.bool, PropTypes.instanceOf(Error)]).isRequired,
}

const mapStateToProps = createStructuredSelector({
  loading: createSelector(({ app }) => app.loading, state => state),
  error: createSelector(({ app }) => app.error, state => state),
})

const mapDispatchToProps = dispatch => bindActionCreators(AppActions, dispatch)

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AppContainer))
