import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { createStructuredSelector, createSelector } from 'reselect'
import { Orcamento } from 'components'
import * as OrcamentoActions from 'actions/orcamento'

class OrcamentoContainer extends Component {
  static propTypes = {
    itens: PropTypes.arrayOf(Object).isRequired,
    listOrcamentos: PropTypes.func.isRequired,
  }

  componentDidMount = () => this.props.listOrcamentos()

  render = () => <Orcamento itens={this.props.itens} />
}

const mapStateToProps = createStructuredSelector({
  itens: createSelector(({ orcamento }) => orcamento.itens, state => state),
})

const mapDispatchToProps = dispatch => bindActionCreators(OrcamentoActions, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(OrcamentoContainer)
