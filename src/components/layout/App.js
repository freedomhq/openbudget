import React from 'react'
import PropTypes from 'prop-types'
import Drawer from 'material-ui/Drawer'
import AppBar from 'material-ui/AppBar'
import Toolbar from 'material-ui/Toolbar'
import List from 'material-ui/List'
import Typography from 'material-ui/Typography'
import Snackbar from 'material-ui/Snackbar'
import { withStyles } from 'material-ui/styles'
import { CircularProgress } from 'material-ui/Progress'
import { menuItens } from './menuItens'

const drawerWidth = 270

const styles = theme => ({
  root: {
    flexGrow: 1,
    zIndex: 1,
    overflow: 'hidden',
    position: 'relative',
    display: 'flex',
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
  drawerPaper: {
    position: 'relative',
    width: drawerWidth,
  },
  content: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
    padding: theme.spacing.unit * 3,
    minWidth: 0, // So the Typography noWrap works
  },
  toolbar: theme.mixins.toolbar,
})

const App = ({ classes, loading, children }) => (
  <div className={classes.root}>
    <AppBar position="absolute" className={classes.appBar}>
      <Toolbar>
        <Typography variant="title" color="inherit" noWrap>
          OpenBudget
        </Typography>
      </Toolbar>
    </AppBar>
    <Drawer variant="permanent" classes={{ paper: classes.drawerPaper }}>
      <div className={classes.toolbar} />
      <List>{menuItens}</List>
    </Drawer>
    <main className={classes.content}>
      <div className={classes.toolbar} />
      {children}
    </main>
    <Snackbar
      open={loading}
      message={
        <Typography id="message-id" variant="title" color="inherit">
          Carregando...
        </Typography>
      }
      action={<CircularProgress />}
      anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
      SnackbarContentProps={{ 'aria-describedby': 'message-id' }}
    />
  </div>
)

App.propTypes = {
  classes: PropTypes.objectOf(Object).isRequired,
  children: PropTypes.element.isRequired,
  loading: PropTypes.bool.isRequired,
}

export default withStyles(styles)(App)
