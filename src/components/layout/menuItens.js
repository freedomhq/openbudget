// This file is shared across the demos.

import React from 'react'
import { Link } from 'react-router-dom'
import { ListItem, ListItemIcon, ListItemText } from 'material-ui/List'
import DashboardIcon from 'material-ui-icons/Dashboard'
import AttachMoney from 'material-ui-icons/AttachMoney'
import AssignmentIcon from 'material-ui-icons/Assignment'

export const menuItens = (
  <div>
    <ListItem button component={Link} to="/">
      <ListItemIcon>
        <DashboardIcon />
      </ListItemIcon>
      <ListItemText primary="Dashboard" />
    </ListItem>
    <ListItem button component={Link} to="/orcamentos">
      <ListItemIcon>
        <AttachMoney />
      </ListItemIcon>
      <ListItemText primary="Orçamentos" />
    </ListItem>
    <ListItem button component={Link} to="/precos">
      <ListItemIcon>
        <AssignmentIcon />
      </ListItemIcon>
      <ListItemText primary="Lista de preços" />
    </ListItem>
  </div>
)
