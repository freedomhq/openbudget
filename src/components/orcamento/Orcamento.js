import React from 'react'
import PropTypes from 'prop-types'
import Paper from 'material-ui/Paper'
import pure from 'recompose/pure'
import {
  SortingState,
  IntegratedSorting,
  PagingState,
  IntegratedPaging,
  SearchState,
  IntegratedFiltering,
} from '@devexpress/dx-react-grid'
import { Grid, Table, TableHeaderRow, PagingPanel, Toolbar, SearchPanel } from '@devexpress/dx-react-grid-material-ui'

const Orcamento = ({ itens }) => {
  const columns = [
    { name: 'cliente', title: 'Cliente' },
    { name: 'evento', title: 'Evento' },
    { name: 'dataCriacao', title: 'Orçado em' },
    { name: 'dataEvento', title: 'Data do evento' },
    { name: 'dataValidade', title: 'Valido até' },
    { name: 'email', title: 'Email' },
  ]

  return (
    <Paper>
      <Grid rows={itens} columns={columns}>
        <SearchState />
        <IntegratedFiltering />
        <SortingState defaultSorting={[{ columnName: 'cliente', direction: 'asc' }]} />
        <IntegratedSorting />
        <PagingState defaultCurrentPage={0} pageSize={15} />
        <IntegratedPaging />
        <Table />
        <TableHeaderRow showSortingControls />
        <Toolbar />
        <SearchPanel messages={{ searchPlaceholder: 'Pesquisar...' }} />
        <PagingPanel />
      </Grid>
    </Paper>
  )
}

Orcamento.propTypes = {
  itens: PropTypes.objectOf(Object).isRequired,
}

export default pure(Orcamento)
