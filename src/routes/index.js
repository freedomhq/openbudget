import React from 'react'
import { BrowserRouter as Router } from 'react-router-dom'
import { renderRoutes } from 'react-router-config'
import { AppContainer } from 'containers'
import styled from 'styled-components'
import CssBaseline from 'material-ui/CssBaseline'
import { MuiThemeProvider, createMuiTheme } from 'material-ui/styles'
import teal from 'material-ui/colors/teal'
import routes from 'routes/routes'

const theme = createMuiTheme({
  palette: {
    type: 'light',
    primary: teal,
  },
})

const Container = styled.div``

const Routes = () => (
  <Router>
    <Container>
      <CssBaseline />
      <MuiThemeProvider theme={theme}>
        <AppContainer>{renderRoutes(routes)}</AppContainer>
      </MuiThemeProvider>
    </Container>
  </Router>
)

export default Routes
