import { OrcamentoContainer } from 'containers'

const routes = [
  {
    path: '/orcamentos',
    exact: true,
    component: OrcamentoContainer,
  },
]

export default routes
