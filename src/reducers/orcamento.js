import { handleActions } from 'redux-actions'
import { DISPLAY_ORCAMENTOS } from 'constants/ActionTypes'

const initialState = {
  itens: [],
}

export default handleActions(
  {
    [DISPLAY_ORCAMENTOS]: (state, action) => ({
      ...state,
      loading: false,
      itens: action.payload.itens,
    }),
  },
  initialState
)
