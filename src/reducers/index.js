import { combineReducers } from 'redux'
import app from './app'
import orcamento from './orcamento'

const rootReducer = combineReducers({
  app,
  orcamento,
})

export default rootReducer
