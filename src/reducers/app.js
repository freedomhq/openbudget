import { START_LOADING, STOP_LOADING, DISPLAY_ERROR, CLEAR_ERROR } from 'constants/ActionTypes'
import { handleActions } from 'redux-actions'

const initialState = {
  loading: false,
  error: false,
}

export default handleActions(
  {
    [START_LOADING]: state => ({
      ...state,
      loading: true,
    }),
    [STOP_LOADING]: state => ({
      ...state,
      loading: false,
    }),
    [DISPLAY_ERROR]: (state, action) => ({
      ...state,
      loading: false,
      error: action.payload.error,
    }),
    [CLEAR_ERROR]: state => ({
      ...state,
      error: false,
    }),
  },
  initialState
)
