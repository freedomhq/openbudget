export default class Datasource {
  findAll = () =>
    new Promise((resolve, reject) => {
      this.datasource
        .allDocs({ include_docs: true, descending: true })
        .then(docs => {
          resolve(docs.rows.map(document => document.doc))
        })
        .catch(err => {
          reject(err)
        })
    })

  findOne = id =>
    new Promise((resolve, reject) => {
      this.datasource
        .get(id)
        .then(doc => {
          resolve(doc)
        })
        .catch(err => {
          reject(err)
        })
    })

  create = doc =>
    new Promise((resolve, reject) => {
      this.datasource
        .put(doc)
        .then(createdDoc => {
          resolve(createdDoc)
        })
        .catch(err => {
          reject(err)
        })
    })
}
