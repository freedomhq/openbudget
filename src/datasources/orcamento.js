import PouchDB from 'pouchdb'
import Datasource from 'datasources/datasource'

const db = new PouchDB('orcamento')
const remote = new PouchDB('http://localhost:5984/orcamento')
const syncHandler = db.sync(remote, {
  live: true,
  retry: true,
})
syncHandler.on('complete', info => {
  console.log(info)
})

export default class Orcamento extends Datasource {
  datasource = db
}
