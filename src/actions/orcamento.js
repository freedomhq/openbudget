import { NEW_ORCAMENTO, CREATE_ORCAMENTO, EDIT_ORCAMENTO, DISPLAY_ORCAMENTOS } from 'constants/ActionTypes'
import { createAction } from 'redux-actions'
import { Orcamento } from 'datasources'
import * as AppActions from './app'

export const newOrcamento = createAction(NEW_ORCAMENTO)
export const createOrcamento = createAction(CREATE_ORCAMENTO, itens => ({ itens }))
export const editOrcamento = createAction(EDIT_ORCAMENTO, itens => ({ itens }))
export const displayOrcamentos = createAction(DISPLAY_ORCAMENTOS, itens => ({ itens }))
export const listOrcamentos = () => dispatch => {
  dispatch(AppActions.startLoading())
  new Orcamento()
    .findAll()
    .then(itens => dispatch(displayOrcamentos(itens)))
    .then(() => dispatch(AppActions.stopLoading()))
    .catch(error => dispatch(AppActions.displayError(error)))
}
