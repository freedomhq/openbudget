import { START_LOADING, STOP_LOADING, DISPLAY_ERROR, CLEAR_ERROR } from 'constants/ActionTypes'
import { createAction } from 'redux-actions'

export const startLoading = createAction(START_LOADING)
export const stopLoading = createAction(STOP_LOADING)
export const displayError = createAction(DISPLAY_ERROR, error => ({ error }))
export const clearError = createAction(CLEAR_ERROR)
